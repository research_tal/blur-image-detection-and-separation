# python blur_detection.py --images images

#import packages
from imutils import paths
import argparse
import cv2
import sys
import os
import shutil


def variance_of_laplacian(image):
    #computing the Laplacian of the image and then return the focus
    return cv2.Laplacian(image, cv2.CV_64F).var()


#constructing the argument parse and parsing the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images", required=True, help="path to input directory of images")
ap.add_argument("-t", "--threshold", type=float, default=500.0, help="focus measures that fall below this value will be considered blurry")
args = vars(ap.parse_args())

#Creating Folder
s=args["images"]
os.mkdir(os.path.join(s, 'Blurry Images'))
os.mkdir(os.path.join(s, 'Not Blurry Images'))


#looping over the input images
for imagePath in paths.list_images(args["images"]):
    #load the image, convert it to grayscle and compute the focus measure using Variance of Laplacian method
    image = cv2.imread(imagePath)
    width = int(image.shape[1]*25/100)
    height = int(image.shape[0]*25/100)
    dim = (width, height)
    resized_image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
    fm = variance_of_laplacian(gray)
    #text = "NOT BLURRY"

    #if the focus measure is less than the threshold, the image should be considered blurry
    if fm<args["threshold"]:
        dst = os.path.join(s, 'Blurry Images')
        shutil.copy(imagePath, dst)
    else:
        dst = os.path.join(s, 'Not Blurry Images')
        shutil.copy(imagePath, dst)
print("Transfer Done!!")
        
        #text = "BLURRY"

    #show the image
    #cv2.putText(image, "{}: {:.2f}".format(text, fm), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
    #cv2.imshow("Image", image)
    #key = cv2.waitKey(0)
