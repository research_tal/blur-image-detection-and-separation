from tkinter import *
import tkinter.messagebox

from imutils import paths

import cv2
import sys
import os
import shutil


def variance_of_laplacian(image):
    #computing the Laplacian of the image and then return the focus
    return cv2.Laplacian(image, cv2.CV_64F).var()
def processing():
    threshold = 500
    path=Entry.get(E1)
    #Creating Folder
    os.mkdir(os.path.join(path, 'Blurry Images'))
    os.mkdir(os.path.join(path, 'Not Blurry Images'))
    #looping over the input images
    for imagePath in paths.list_images(path):
        #load the image, convert it to grayscle and compute the focus measure using Variance of Laplacian method
        image = cv2.imread(imagePath)
        width = int(image.shape[1]*25/100)
        height = int(image.shape[0]*25/100)
        dim = (width, height)
        resized_image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
        gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
        fm = variance_of_laplacian(gray)
    

        #if the focus measure is less than the threshold, the image should be considered blurry
        if fm<threshold:
            dst = os.path.join(path, 'Blurry Images')
            shutil.copy(imagePath, dst)
        else:
            dst = os.path.join(path, 'Not Blurry Images')
            shutil.copy(imagePath, dst)

    
top = tkinter.Tk()
L1 = Label(top, text="Blurry Image Seprator",).grid(row=0,column=1)
L2 = Label(top, text="Image Directory Path",).grid(row=1,column=0)

E1 = Entry(top, bd=5)
E1.grid(row=1,column=1)

B=Button(top, text="Submit",command=processing).grid(row=2,column=1)
top.mainloop()
